# Differentiation in PyTorch
# Objective

#     How to perform differentiation in pytorch.

# Table of Contents

# In this lab, you will learn the basics of differentiation.

#     Derivatives
#     Partial Derivatives

# Estimated Time Needed: 25 min
# Preparation

# The following are the libraries we are going to use for this lab.

# These are the libraries will be useing for this lab.
import torch
import matplotlib.pylab as plt

# Derivatives

# Let us create the tensor x and set the parameter requires_grad to true
# because you are going to take the derivative of the tensor.

# Create a tensor x
x = torch.tensor(2.0, requires_grad = True)
print("The tensor x: ", x)

# Then let us create a tensor according to the equation 𝑦=𝑥2

# Create a tensor y according to y = x^2
y = x ** 2
print("The result of y = x^2: ", y)

# Then let us take the derivative with respect x at x = 2

# Take the derivative. Try to print out the derivative at the value x = 2
y.backward()
print("The dervative at x = 2: ", x.grad)

# # The preceding lines perform the following operation:
# dy(x)/dx=2𝑥
# dy(x=2)/dx=2(2)=4
print('data:',x.data)
print('grad_fn:',x.grad_fn)
print('grad:',x.grad)
print("is_leaf:",x.is_leaf)
print("requires_grad:",x.requires_grad)
print('data:',y.data)
print('grad_fn:',y.grad_fn)
print('grad:',y.grad)
print("is_leaf:",y.is_leaf)
print("requires_grad:",y.requires_grad)

# Let us try to calculate the derivative for a more complicated function.

# Calculate the y = x^2 + 2x + 1, then find the derivative
x = torch.tensor(2.0, requires_grad = True)
y = x ** 2 + 2 * x + 1
print("The result of y = x^2 + 2x + 1: ", y)
y.backward()
print("The dervative at x = 2: ", x.grad)

# The function is in the following form: 𝑦=𝑥2+2𝑥+1

# The derivative is given by:
# dy(x)/dx=2𝑥+2
# dy(x=2)/dx=2(2)+2=6

# Practice

# Determine the derivative of 𝑦=2𝑥3+𝑥
# at 𝑥=1

# Practice: Calculate the derivative of y = 2x^3 + x at x = 1
x = torch.tensor(1.0, requires_grad=True)
y = 2 * x ** 3 + x
y.backward()
x.grad()
​



# We can implement our own custom autograd Functions by subclassing
# torch.autograd.Function and implementing the forward and backward passes
# which operate on Tensors

class SQ(torch.autograd.Function):

    @staticmethod
    def forward(ctx,i):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        result = i ** 2
        ctx.save_for_backward(i)
        return result
​
    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        i, = ctx.saved_tensors
        grad_output = 2 * i
        return grad_output

# We can apply it the function
x=torch.tensor(2.0,requires_grad=True )
sq=SQ.apply

y=sq(x)
y
print(y.grad_fn)
y.backward()
x.grad

# Partial Derivatives

# We can also calculate Partial Derivatives. Consider the function:
# 𝑓(𝑢,𝑣)=𝑣𝑢+𝑢2

# Let us create u tensor, v tensor and f tensor

# Calculate f(u, v) = v * u + u^2 at u = 1, v = 2
u = torch.tensor(1.0,requires_grad=True)
v = torch.tensor(2.0,requires_grad=True)
f = u * v + u ** 2
print("The result of v * u + u^2: ", f)

# This is equivalent to the following:

# 𝑓(𝑢=1,𝑣=2)=(2)(1)+12=3

# Now let us take the derivative with respect to u:

# Calculate the derivative with respect to u
f.backward()
print("The partial derivative with respect to u: ", u.grad)

# the expression is given by:

# ∂f(u,v)∂𝑢=𝑣+2𝑢

# ∂f(u=1,v=2)∂𝑢=2+2(1)=4

# Now, take the derivative with respect to v:

# Calculate the derivative with respect to v
print("The partial derivative with respect to u: ", v.grad)

# The equation is given by:

# ∂f(u,v)∂𝑣=𝑢

# ∂f(u=1,v=2)∂𝑣=1

# Calculate the derivative with respect to a function with multiple values as
# follows. You use the sum trick to produce a scalar valued function and then
# take the gradient:

# Calculate the derivative with multiple values
x = torch.linspace(-10, 10, 10, requires_grad = True)
Y = x ** 2
y = torch.sum(x ** 2)

# We can plot the function and its derivative

# Take the derivative with respect to multiple value. Plot out the function and
# its derivative
y.backward()
plt.plot(x.detach().numpy(), Y.detach().numpy(), label = 'function')
plt.plot(x.detach().numpy(), x.grad.detach().numpy(), label = 'derivative')
plt.xlabel('x')
plt.legend()
plt.show()

# The orange line is the slope of the blue line at the intersection point,
# which is the derivative of the blue line.

# The method detach() excludes further tracking of operations in the graph, and
# therefore the subgraph will not record operations. This allows us to then
# convert the tensor to a numpy array. To understand the sum operation Click
# Here

# The relu activation function is an essential function in neural networks. We
# can take the derivative as follows:

​# Take the derivative of Relu with respect to multiple value. Plot out the function and its derivative
​x = torch.linspace(-10, 10, 1000, requires_grad = True)
Y = torch.relu(x)
y = Y.sum()
y.backward()
plt.plot(x.detach().numpy(), Y.detach().numpy(), label = 'function')
plt.plot(x.detach().numpy(), x.grad.detach().numpy(), label = 'derivative')
plt.xlabel('x')
plt.legend()
plt.show()
y.grad_fn

# Practice

# Try to determine partial derivative 𝑢
# of the following function where 𝑢=2 and 𝑣=1: 𝑓=𝑢𝑣+(𝑢𝑣)2

# # Practice: Calculate the derivative of f = u * v + (u * v) ** 2 at u = 2, v = 1
u = torch.tensor(2.0, requires_grad=True)
v = torch.tensor(1.0, requires_grad=True)
f = u * v + (u * v) ** 2
f.backward()
print("the result is ", u.grad)
