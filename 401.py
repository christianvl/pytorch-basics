# Multiple Linear Regression
# Objective

#     How to make the prediction for multiple inputs.
#     How to use linear class to build more complex models.
#     How to build a custom module.

# Table of Contents

# In this lab, you will review how to make a prediction in several different ways by using PyTorch.

#     Prediction
#     Class Linear
#     Build Custom Modules

# Estimated Time Needed: 15 min
# Preparation

# Import the libraries and set the random seed.

# Import the libraries and set the random seed
from torch import nn
import torch
torch.manual_seed(1)

# Prediction

# Set weight and bias.

# Set the weight and bias
w = torch.tensor([[2.0], [3.0]], requires_grad=True)
b = torch.tensor([[1.0]], requires_grad=True)

# Define the parameters. torch.mm uses matrix multiplication instead of scaler multiplication.

# Define Prediction Function
def forward(x):
    yhat = torch.mm(x, w) + b
    return yhat

# The function forward implements the following equation:
# Matrix Linear Regression

# If we input a 1x2 tensor, because we have a 2x1 tensor as w, we will get a 1x1 tensor:

# Calculate yhat
x = torch.tensor([[1.0, 2.0]])
yhat = forward(x)
print("The result: ", yhat)

# Linear Regression Matrix Sample One
# Each row of the following tensor represents a sample:

# Sample tensor X
X = torch.tensor([[1.0, 1.0], [1.0, 2.0], [1.0, 3.0]])

# Make the prediction of X
yhat = forward(X)
print("The result: ", yhat)

# Class Linear

# We can use the linear class to make a prediction. You'll also use the linear class to build more complex models.

# Let us create a model.

# Make a linear regression model using build-in function
model = nn.Linear(2, 1)

# Make a prediction with the first sample:

# Make a prediction of x
yhat = model(x)
print("The result: ", yhat)

# Predict with multiple samples X:

# Make a prediction of X
yhat = model(X)
print("The result: ", yhat)

# The function performs matrix multiplication as shown in this image:
# Linear Regression Matrix Sample One
# Build Custom Modules

# Now, you'll build a custom module. You can make more complex models by using this method later.

# Create linear_regression Class
class linear_regression(nn.Module):
    # Constructor
    def __init__(self, input_size, output_size):
        super(linear_regression, self).__init__()
        self.linear = nn.Linear(input_size, output_size)

    # Prediction function
    def forward(self, x):
        yhat = self.linear(x)
        return yhat

# Build a linear regression object. The input feature size is two.
model = linear_regression(2, 1)

# This will input the following equation:
# Matrix Linear Regression

# You can see the randomly initialized parameters by using the parameters() method:

# Print model parameters
print("The parameters: ", list(model.parameters()))

# You can also see the parameters by using the state_dict() method:

# Print model parameters
print("The parameters: ", model.state_dict())

# Now we input a 1x2 tensor, and we will get a 1x1 tensor.

# Make a prediction of x
yhat = model(x)
print("The result: ", yhat)

# The shape of the output is shown in the following image:
# Matrix Linear Regression

# Make a prediction for multiple samples:

# Make a prediction of X
yhat = model(X)
print("The result: ", yhat)

# The shape is shown in the following image:
# Multiple Samples Linear Regression
# Practice

# Build a model or object of type linear_regression. Using the linear_regression object will predict the following tensor:

# Practice: Build a model to predict the follow tensor.
X = torch.tensor([[11.0, 12.0, 13, 14], [11, 12, 13, 14]])

model = nn.Linear(4, 1)
yhat = model(X)
print("yhat is: ", yhat)
