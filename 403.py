
# Objective

#     How to make a prediction using multiple samples.

# Table of Contents

# In this lab, we will review how to make a prediction for Linear Regression with Multiple Output.
# Build Custom Modules

# Estimated Time Needed: 15 min

# Class Linear
from torch import nn
import torch

# Set the random seed:
torch.manual_seed(1)

class linear_regression(nn.Module):
    def __init__(self,input_size,output_size):
        super(linear_regression,self).__init__()
        self.linear=nn.Linear(input_size,output_size)

    def forward(self,x):
        yhat=self.linear(x)
        return yhat

# create a linear regression object, as our input and output will be two we set the parameters accordingly
model=linear_regression(1,10)
model(torch.tensor([1.0]))

# we can use the diagram to represent the model or object
# Image

# we can see the parameters
list(model.parameters())

# we can create a tensor with two rows representing one sample of data
x=torch.tensor([[1.0]])

# we can make a prediction
yhat=model(x)
yhat

# each row in the following tensor represents a different sample
X=torch.tensor([[1.0],[1.0],[3.0]])

# we can make a prediction using multiple samples
Yhat=model(X)
Yhat

# the following figure represents the operation, where the red and blue
# represents the different parameters, and the different shades of green
# represent different samples.
