# Table of Contents

# In this lab, we will look at autoencoders as matrices. We will see how
# changing the shape in the shape of the latent space will changing the shape
# output.

#     Autoencoders with 2D Latent Space as Matrice
#     Autoencoders with 1D Latent Space as Matrices

# Estimated Time Needed: 30 min
# Autoencoders with 2D Latent Space = as Matrices

# Create an Autoencoder custom module or class:
class AutoEncoder(nn.Module):
    def __init__(self, input_dim=256, encoding_dim=32):
        super(AutoEncoder, self).__init__()
        self.encoder = nn.Linear(input_dim,encoding_dim,bias=False)
        self.decoder = nn.Linear(encoding_dim,input_dim,bias=False)
    def forward(self, x):
        x=self.encoder(x)
        x=self.decoder(x)
        return x

# We Create an Autoencoder object with a 2D input and 2D latent space as shown in the image.
# cognitiveclass.ai logo
auto_encoder_2Dcode=AutoEncoder(2,2)
auto_encoder_2Dcode

# As the weights are randomly initialized, we set them the orthogonal basis in
# the video for the encoder. As PyTorch treats the input as rows, we transpose
# all the wights.
print("encoder weight installation", auto_encoder_2Dcode.state_dict()['encoder.weight'])
​
W=torch.tensor([[1/2**(0.5),1/2**(0.5)],[-1/2**(0.5),1/2**(0.5)]])
auto_encoder_2Dcode.state_dict()['encoder.weight'].data[:,:]=W
print("new encoder weight ", auto_encoder_2Dcode.state_dict()['encoder.weight'])

# we will do the same for the decoder;
auto_encoder_2Dcode.state_dict()['decoder.weight'].data[:,:]=torch.transpose(W,0,1)
auto_encoder_2Dcode.state_dict()

# we can get the encoder output or code as follows:
x=torch.tensor([[1.0,1.0]])
​
z=auto_encoder_2Dcode.encoder(x)
z
# we can generate the outputs; it's identical to the input:
x_hat=auto_encoder_2Dcode.decoder(z)
x_hat

# We can produce the output by calling the forward function:
x_hat=auto_encoder_2Dcode(x)
x_hat

# we can generate the code for multiple samples:
X=torch.tensor([[1.0,0],[0,1],[-1.0,0],[0,-1.0],[1,1],[-1,1],[1,-1],[-1,-1]])
Z=auto_encoder_2Dcode.encoder(X)
Z

# We see the output is the same as the code:
Xhat=auto_encoder_2Dcode(X)
print('Xhat:')
print(Xhat)
print('X')
print(X)

# We see the output is the same as the code:
Xhat=auto_encoder_2Dcode(X)
print('Xhat:')
print(Xhat)
print('X')
print(X)

# The following plot shows the input space and tensors or vectors on the left.
# The latent space and the code are on the right. Finally we have the code. The
# corresponding samples are/' colour coded accordingly.
colors=['r','r','b','c','k','k','b','g','r']
​
for x,z,xhat,c in zip(X,Z,Xhat,colors):
    plt.subplot(131)
    plt.quiver([0],[0],x[0].numpy(),x[1].numpy(),scale=5,color=c)
    plt.title(' input space x')
    plt.subplot(132)
    plt.plot(z[0].detach().numpy(),z[1].detach().numpy(),c+'o')
    plt.quiver([0],[0],0,1,scale=5,color='k')
    plt.quiver([0],[0],1,0,scale=5,color='k')
    plt.title('latent space z')
    plt.subplot(133)
    plt.quiver([0],[0],x[0].numpy(),x[1].numpy(),scale=5,color=c)
​
    plt.title('output xhat')
plt.show()

# Autoencoders with 1D Latent Space as Matrices
# We Create an Autoencoder object with a 2D input and 1D latent space.
# cognitiveclass.ai logo
auto_encoder_1Dcode=AutoEncoder(2,1)
auto_encoder_1Dcode

# we can plot the data.
W=torch.tensor([[1/2**(0.5),1/2**(0.5)]])
auto_encoder_2Dcode.state_dict()['encoder.weight'].data[:,:]=W
​
auto_encoder_2Dcode.state_dict()['decoder.weight'].data[:,:]=torch.transpose(W,0,1)
z=auto_encoder_1Dcode.encoder(torch.tensor([[1.0,1.0]]))
z

# we can generate the outputs; it's identical to the input:
x_hat=auto_encoder_1Dcode.decoder(z)
x_hat

# We can produce the output by calling the forward function:
x_hat=auto_encoder_1Dcode(x)
x_hat

# we can generate the code for multiple samples:
X=torch.tensor([[1.0,0],[0,1],[-1,0],[0,-1],[1,1],[-1,1],[1,-1],[-1,-1]])
Z=auto_encoder_1Dcode.encoder(X)
Z

# The output is not the same, as there is not enough information to pass-through there encoder. As a result, all the output is vectors are scaler multiples of the vector [1,1]
.
Xhat=auto_encoder_1Dcode(X)
print('Xhat:')
print(Xhat)
print('X')
print(X)

# The following plot shows the input space and tensors or vectors on the left.
# The latent space and the code are on the right. Finally we have the code each
# point vector is mapped to a point on a 1D line. Finally, we have the output
# all the vectors span the line equivalent to 𝑦=𝑥
# or a scaler multiple of the vector [1,1]
# . The corresponding samples are/' colour coded accordingly.
colors=['r','r','b','c','k','k','b','g','r']
​
for x,z,xhat,c in zip(X,Z,Xhat,colors):
    plt.subplot(131)
    plt.quiver([0],[0],x[0].numpy(),x[1].numpy(),scale=5,color=c)
    plt.title(' input space x')
    plt.subplot(132)
    plt.plot(z[0].detach().numpy(),0,c+'o')
​
    plt.title('latent space z')
    plt.subplot(133)
    plt.quiver([0],[0],10*xhat[0].detach().numpy(),10*xhat[1].detach().numpy(),scale=5,color=c)
​
    plt.title('output xhat')
plt.show()
