# cognitiveclass.ai logo
# Principal Component Analysis (PCA) and Autoencoders
# Table of Contents

# In this lab, we will compare Principal Component Analysis (PCA), we will show how it can reduce the dimensions of the data..

#     Preparation
#     PCA 2D
#     Autoencoders 2D
#     PCA 3D
#     Autoencoders 3D

# Estimated Time Needed: 30 min
# Preparation

# We'll need the following labs and functions:

# These are the libraries we are going to use in the lab.

​
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import proj3d
import seaborn as sns
import torch
import torch.nn as nn
from matplotlib.patches import FancyArrowPatch

# Simple autoencoder module or class
class AutoEncoder(nn.Module):
    def __init__(self, input_dim=2, encoding_dim=32):
        super(AutoEncoder, self).__init__()
        self.encoder = nn.Linear(input_dim,encoding_dim)
        self.decoder = nn.Linear(encoding_dim,input_dim)
    def forward(self, x):
        x=self.encoder(x)
        x=self.decoder(x)
        return x

# function to train an autoencoder:
def train_model(model,X,optimizer,n_epochs=4):
    #global variable
    cost_list_training =[]
    cost_list_validation =[]
    for epoch in range(n_epochs):
        cost_training=0
        for x in X:
            model.train()
            optimizer.zero_grad()
            xhat = model(x)
            loss = criterion(xhat, x)
            loss.backward()
            optimizer.step()
            cost_training+=loss.data
        cost_list_training.append(cost_training)
    return cost_list_training

# This function will plot the data in 2D-data.
def plot_data(X,Xhat,vec=False,label_X="data",label_Xhat="transformed data",lable_vec="parameter vector"):
​
    if type(X) is torch.Tensor:
        plt.scatter(X[:, 0].numpy(), X[:, 1].numpy(),label=label_X)
    if type(Xhat) is torch.Tensor:
        plt.scatter(Xhat[:, 0].numpy(), Xhat[:, 1].numpy(),label=label_Xhat)
    if type(vec) is torch.Tensor:
        plt.xlabel("q_{1}")
        plt.ylabel("q_{2}")
        plt.quiver([0],[0],vec[:,0],vec[:,1],label=lable_vec)
    plt.legend()
    plt.show()

# This function will generate 3D data from a plane and add noise.
def GenerateData(ProjectionMatrix=[[1,-1,0],[2,1,0],[0,0,0]],Noise_std=0.1,Nsamples=1000):
    import numpy as np
    np.random.seed(0)
​
    Max=1
    Min=-1
    #Create a projection matrix
    ProjectionMatrix=np.array([[1,-1,0],[2,1,0],[0,0,0]])
    #Acreate some noise and add to data
    Noise=Noise_std*np.random.normal(loc=0.0, scale=1, size=(Nsamples,3))
    #Create some uniform data and  projection onto plane
    TureData=np.dot(ProjectionMatrix,np.random.uniform(low=Min, high=Max, size=(Nsamples,3)).T ).T
    TureData=TureData-TureData.mean( axis=0)
    #add noise
    CorruptedData =TureData+Noise
    CorruptedData =CorruptedData -CorruptedData .mean( axis=0)
    return TureData,CorruptedData

# This function will plot the 3D data.
def PlotData3D(Data,Data1,Name1="Data 1",Name2="Data 2",BasisVectors=None):
    fig = plt.figure(figsize=(15,15))
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Data[:,0], Data[:,1], Data[:,2], 'x', markersize=10, color='b', alpha=1,label=Name1)
    ax.plot(Data1[:,0], Data1[:,1], Data1[:,2], 'o', markersize=10, color='r', alpha=0.5,label=Name2)
    if type(BasisVectors)==torch.Tensor:
        for q in torch.transpose(BasisVectors,0,1):
            print(q)
            a = Arrow3D([0,q[0].numpy()], [0, q[1].numpy()], [0, q[2].numpy()], mutation_scale=20, lw=3, arrowstyle="-|>", color="k")
            ax.add_artist(a)
    ax.set_zlim([-2,2])
    ax.set_xlim([-2,2])
    ax.set_ylim([-2,2])
    ax.legend(shadow=True)

# This function will create the arrows in 3D to represent vectors
class Arrow3D(FancyArrowPatch):
    #code by CT Zhu
    #https://stackoverflow.com/users/2487184/ct-zhu
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs
​
    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)
# PCA 2D In this section, we create a dataset object that uses Principal
# component analysis (PCA). We find the projection of the data on the
# eigenvectors of the covariance matrix 𝐐 , as shown below. We zero center the
# data. We generate 2D data that is correlated.
samples=200
u=torch.tensor([[1.0,1.0],[0.10,-0.10]])/(2)**(0.5)
X=torch.mm(4*torch.randn(samples,2),u)+2
plt.scatter(X[:, 0].numpy(), X[:, 1].numpy())
plt.show()
​
# From now on we will deal with zero zero centered data
# 𝐱=𝐱−𝝁
X_old=X
X=X-X.mean(dim=0)
​
# We plot the date before we subtract and after we subtract the mean.
plot_data(X_old,X,label_X="original data",label_Xhat="zero-mean")

# 1𝑁𝐗𝑇𝐗=𝐐𝚲𝐐𝑇
# We calculate the empirical covariance matrix.
# 1𝑁𝐗𝑇𝐗
Cov=torch.mm(torch.t(X),X)/X.shape[0]
Cov

# We obtain the eigenvectors 1𝑁𝐗𝑇𝐗=𝐐𝚲𝐐𝑇
eigenvalues,eigenvectors=torch.eig(Cov,True)
eigenvectors

# we have the eigenvalues:
eigenvalues

# we can plot the eigenvectors
row_vec=torch.t(eigenvectors).numpy()
plt.scatter(X[:, 0].numpy(), X[:, 1].numpy(),label="data")
plt.quiver([0],[0],row_vec[:,0],row_vec[:,1],label="Eigen vectors")
plt.xlabel("x_{1}")
plt.ylabel("x_{2}")
plt.legend()
plt.show()
​
# We find the projection the eigenvectors: 𝑍=𝐗𝐐
Z=torch.mm(X,eigenvectors)
plot_data(X,Z,eigenvectors,label_X="data",label_Xhat="Z",lable_vec="q")
torch.mm(torch.transpose(Z,0,1),Z)/Z.size()[0]

# We can also find the 𝐗̂ , as we are using all the vectors, we have a perfect reconstruction. 𝐗̂ =𝐐𝐙
Xhat=torch.mm(Z,torch.transpose(eigenvectors,0,1))
plot_data(X,Xhat,eigenvectors,label_X="X",label_Xhat="Xhat",lable_vec="parameter vector")

# We can also find the max eigenvalue
max_eigenvalue=torch.argmax(eigenvalues[:,0])
max_eigenvalue

# we find the corresponding max eigenvector
max_eigenvector=eigenvectors[:,max_eigenvalue]

# To keep the tensors the same shape we simply copy the eigenvector to a new
# tensor. This behaves like setting the other eigenvector to zero.
new_Q=torch.zeros(2,2)
new_Q[:,0]=max_eigenvector

# We find Xhat, this time as it’s an approximation all the points fall on the
# line that is a scaler multiple of are eigenvector.
Z=torch.mm(X,new_Q)
Xhat=torch.mm(Z,torch.transpose(new_Q,0,1))
plot_data(X,Xhat,eigenvectors,label_X="data",label_Xhat="Xhat",lable_vec="parameter vector")

# Autoencoders 2D We do the same for an autoencoder, but instead of only
# selecting one eigenvector we set the code length to be one.
model=AutoEncoder(2,1)

# Just like a neural network we train the encoder using gradient descent.
learning_rate = 0.01
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
cost_list_training=train_model(model,X,optimizer,n_epochs=4)

# We can make a prediction for Xhat and plot it.
Xhat=model(X)
plot_data(X,model(X).detach(),eigenvectors,label_X="data",label_Xhat="Xhat",lable_vec="parameter vector")

# We see that the output is similar to PCA. The data spans a line.
# PCA 3D
# Let’s do the same for three dimensional points
# $\mathbf{\hat{X}}=\mathbf{X} \mathbf{Q} \mathbf{\Lambda}^{-1/2} $

# Let’s do the same for three dimensional points. We will generate some data
# from a plane and add noise. We will convert it to a PyTorch tensor and
# subtract the mean.
TureData,CorruptedData=GenerateData(ProjectionMatrix=[[1,-1,0],[2,1,0],[0,0,0]],Noise_std=0.1,Nsamples=1000)
TureData
X=torch.from_numpy(CorruptedData).float()
X=X-X.mean(dim=0)

# We can plot out the original points in blue and overlay the points with added noise in red.
PlotData3D(TureData,CorruptedData,Name1="Orignal Data ",Name2="Corrupted Data ",BasisVectors=torch.eye(3))
X

# We can determine covariance matrix and it eigenvalues and eigenvectors
Cov=torch.mm(torch.t(X),X)/X.shape[0]
eigenvalues,eigenvectors=torch.eig(Cov,True)

# We can verify Orthogonal i.e parallel, more specifically orthonormal basis.
torch.mm(torch.transpose(eigenvectors,0,1),eigenvectors)

# We find the index of the first two largest eigenvectors:
indexs=set([0,1,2] )
min_eigenvalues=torch.argmin(eigenvalues[:,0]).item()
indexs.remove(min_eigenvalues)
indexs=list(indexs)
eigenvectors

# To keep the tensors the same shape we simply copy the top eigenvector to a
# new tensor. This behaves like setting the other eigenvector to zero.
new_Q=torch.zeros(3,3)
for i,index in enumerate(indexs):
    new_Q[:,i]=eigenvectors[:,index]

# We find the code and the prediction $\mathbf{\hat{X}}$.
Z=torch.mm(X,new_Q)
Z
Xhat=torch.mm(Z,torch.transpose(new_Q,0,1))

# We compare the output compared to the data points with noise. We see the output points fall in the line.
PlotData3D(Xhat.numpy(),CorruptedData,Name1="Xhat",Name2="Data with Noise",BasisVectors=new_Q)

# Autoencoder in 3D
# We can perform a similar process with an Autoencoder but
# the vectors are not the same. We can verify this by showing that the basis
# are not orthogonal.
model=AutoEncoder(3,3)
learning_rate = 0.01
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
cost_list_training=train_model(model,X,optimizer,n_epochs=4)
Xhat=model(X)
torch.mm(torch.transpose(model.state_dict()['encoder.weight'],0,1), model.state_dict()['encoder.weight'])
​
# We can use the autoencoder for dimensionality reduction as well, we can reduce the code to two.
model=AutoEncoder(3,2)
learning_rate = 0.01
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
cost_list_training=train_model(model,X,optimizer,n_epochs=4)
model.state_dict()
model.encoder(X)

# We can see the output spans the same plane.
Xhat=model(X)
PlotData3D(Xhat.detach().numpy(),CorruptedData,Name1="Xhat",Name2="Data with Noise",BasisVectors=model.state_dict()['decoder.weight'].detach())
