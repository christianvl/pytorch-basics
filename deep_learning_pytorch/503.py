# Preprocessing with Principle Component Analysis
# Table of Contents

# This notebook contains neural networks trained to predict house prices in King County, Washington, USA based on features of the house. They are trained using different preprocessing methods like PCA and Standardization. They are also trained using different Neural Networks with one featuring batch normalization. We will explore the benefits of data preprocessing on our neural networks and how they affect performance.

#     Utility Functions and Imports
#     Dataset Class
#     Different Neural Networks and Function to Train Neural Network
#     Training and Validate Model

# Estimated Time Needed: 20 min
# Utility Functions and Imports
import pandas as pd
import numpy as np
import torch
from sklearn import preprocessing
from sklearn.decomposition import PCA
from torch.utils.data import Dataset
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.datasets as dsets
import torch.nn.functional as F
import matplotlib.pylab as plt
torch.manual_seed(1)
np.random.seed(1)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

# Here we load the dataset we will use and the already trained models to save time instead of training from scratch.
!wget https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/meet_up/12.02.2020/king_county_house_data.csv
!wget https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/meet_up/12.02.2020/pca_model.pt
!wget https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/meet_up/12.02.2020/regular_model.pt
!wget https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/meet_up/12.02.2020/standardized_model.pt

# Dataset Class
# Turn the csv into a dataframe
df = pd.read_csv('king_county_house_data.csv')
print(df.shape)
df.head()

# Create a PyTorch Dataset object to store our dataframe values in. We create 3
# type of data one for data with PCA, Standardization, and regular data with no
# preprocessing.
class data(Dataset):
    def __init__(self, test=False, regular=False, pca=False, standard=False):
        X = df.drop(columns='price')
        Y = df['price']
        # Split the data into training data and testing data using a 80/20 split
        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=.2, random_state=1)
        x_train = x_train.astype(float)
        x_test = x_test.astype(float)
        if (regular):
            if (test):
                self.x = torch.FloatTensor(x_test.values)
                self.y = torch.FloatTensor(y_test.values).view(-1,1)
            else:
                self.x = torch.FloatTensor(x_train.values)
                self.y = torch.FloatTensor(y_train.values).view(-1,1)
        elif (pca):
            # Performs PCA on the data
            pca = PCA(whiten=True)
            x_train = pca.fit_transform(x_train)
            x_test = pca.transform(x_test)
            if (test):
                self.x = torch.FloatTensor(x_test)
                self.y = torch.FloatTensor(y_test.values).view(-1,1)
            else:
                self.x = torch.FloatTensor(x_train)
                self.y = torch.FloatTensor(y_train.values).view(-1,1)
        else:
            # Performs Standardization on the data
            standard = preprocessing.StandardScaler()
            x_train = standard.fit_transform(x_train)
            x_test = standard.transform(x_test)
            if (test):
                self.x = torch.FloatTensor(x_test)
                self.y = torch.FloatTensor(y_test.values).view(-1,1)
            else:
                self.x = torch.FloatTensor(x_train)
                self.y = torch.FloatTensor(y_train.values).view(-1,1)
​
    # Getter
    def __getitem__(self, index):
        return self.x[index], self.y[index]
    def __len__(self):
        return len(self.x)

# Different Neural Networks and Function to Train Neural Network Here we define
# the Neural Networks we will use. They are both 4 layered networks with 1
# input layer, 1 output layer, and 2 hidden layers. THe number of nodes in each
# layer is 2/3 of the input layer except the output because we are doing
# regression. They both use relu as the activation function. The difference is
# that one uses Batch Normalization and one does not.
class BatchNormalizationNN(nn.Module):
    # Constructor
    def __init__(self):
        super(BatchNormalizationNN, self).__init__()
        # The dataset has 18 features
        self.linear1 = nn.Linear(18, 12)
        self.b1 = nn.BatchNorm1d(12)
        self.linear2 = nn.Linear(12, 8)
        self.b2 = nn.BatchNorm1d(8)
        self.linear3 = nn.Linear(8, 1)
​
    # Prediction
    def forward(self, x):
        y = self.b1(torch.relu(self.linear1(x)))
        y = self.b2(torch.relu(self.linear2(y)))
        y = self.linear3(y)
        return y
class RegularNN(nn.Module):
    # Constructor
    def __init__(self):
        super(RegularNN, self).__init__()
        # The dataset has 18 features
        self.linear1 = nn.Linear(18, 12)
        self.linear2 = nn.Linear(12, 8)
        self.linear3 = nn.Linear(8, 1)
​
    # Prediction
    def forward(self, x):
        y = torch.relu(self.linear1(x))
        y = torch.relu(self.linear2(y))
        y = self.linear3(y)
        return y

# Function performs the training of the models. It is given the training data,
# testing data, criterion (loss function), model(Neural Network), optimizer,
# and epochs (Number of times the model is trained on the entire dataset).
def train(train_loader, test_loader, criterion, model, optimizer, epochs):
    train_loss = []
    test_loss = []
    for epoch in range(epochs):
        print(epoch)
        total_loss = 0
        for i, (x, y) in enumerate(train_loader):
            # Set the model in training mode so it updates values in Batch Normalization
            model.train()
            # Clears the optimizer
            optimizer.zero_grad()
            # Calculates the prediction the model gives
            z = model(x)
            # Calculates the loss between the prediction and the actual value
            loss = criterion(z, y)
            # Calcualates the partial deriviative for each parameter of the Neural Network
            loss.backward()
            # Adjusts the parameters according to the optimizer
            optimizer.step()
            total_loss = total_loss + loss.item()
        train_loss.append(total_loss/(len(train_loader.dataset)/train_loader.batch_size))
        print(total_loss/(len(train_loader.dataset)/train_loader.batch_size))
        total_loss = 0
        for i, (x, y) in enumerate(test_loader):
            # Set the model in evaluation mode so we do not train the Batch Normalization while evaluating our model
            model.eval()
            z = model(x)
            loss = criterion(z, y)
            total_loss = total_loss + loss.item()
        test_loss.append(total_loss/len(test_loader.dataset))
        print(total_loss/len(test_loader.dataset))
    return train_loss, test_loss

# Training and Validate Model
# We have trained the model prior you can load them and use them to run you model.
# Vanilla Neural network
RegularModel = RegularNN()
checkpoint = torch.load('regular_model.pt')
RegularModel.load_state_dict(checkpoint['model_state_dict'])
RegularModel.eval()
regular_train_loss = checkpoint['train_loss']
regular_test_loss = checkpoint['test_loss']
Neural network with Batch Norm
PCAModel = BatchNormalizationNN()
checkpoint = torch.load('pca_model.pt')
PCAModel.load_state_dict(checkpoint['model_state_dict'])
PCAModel.eval()
pca_train_loss = checkpoint['train_loss']
pca_test_loss = checkpoint['test_loss']
Neural network with Batch Norm and Starderdizing the data :
StandardModel = BatchNormalizationNN()
checkpoint = torch.load('standardized_model.pt')
StandardModel.load_state_dict(checkpoint['model_state_dict'])
StandardModel.eval()
stan_train_loss = checkpoint['train_loss']
stan_test_loss = checkpoint['test_loss']

# Training

# The code in the sections below is code to train the models with each type of
# preprocessing. The training code is saved a raw so it does not accidentally
# get run as it would take a lot of time to train. The loss function used is
# SmoothL1Loss and in its current setup it gives the absolute value of the
# error between the prediction and actual value. The optimizer used is
# Adadelta, we use this because we do not need to worry about initializing or
# updating the rate at which the Neural Network learns. If you would like to
# experiment with the Neural Networks please change the type of the training
# block to code using the options at the top.

# Regular Neural Network
# If you would like to train the model yourself. you can convert the following markdown cell to a code.
d_train = data(regular=True, test=False)
train_loader = torch.utils.data.DataLoader(dataset=d_train, batch_size=500)
d_test = data(regular=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
criterion = nn.SmoothL1Loss()
RegularModel = RegularNN()
optimizer = torch.optim.Adadelta(RegularModel.parameters())
regular_train_loss, regular_test_loss = train(train_loader, test_loader, criterion, RegularModel, optimizer, 10)
plt.plot(regular_train_loss)
plt.title("Regular Train")
plt.xlabel("Epochs")
plt.ylabel("Average Loss ")
plt.show()
print(regular_train_loss[-1])
plt.plot(regular_test_loss)
plt.title("Regular Validation Data ")
plt.xlabel("Epochs")
plt.ylabel("Average Loss ")
plt.show()
print(regular_test_loss[-1])
torch.save({
    'epochs': 500,
    'model_state_dict': RegularModel.state_dict(),
    'train_loss': regular_train_loss,
    'test_loss': regular_test_loss
}, 'regular_model.pt')

# PCA and Regular Neural Network
d_train = data(pca=True, test=False)
train_loader = torch.utils.data.DataLoader(dataset=d_train, batch_size=500)
d_test = data(pca=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
criterion = nn.SmoothL1Loss()
PCAModel = BatchNormalizationNN()
optimizer = torch.optim.Adadelta(PCAModel.parameters())
pca_train_loss, pca_test_loss = train(train_loader, test_loader, criterion, PCAModel, optimizer, 60)
plt.plot(pca_train_loss)
plt.title("PCA Train")
plt.xlabel("Epochs")
plt.ylabel("Average Loss")
plt.show()
print(pca_train_loss[-1])
plt.plot(pca_test_loss)
plt.title("PCA Validation Data")
plt.xlabel("Epochs")
plt.ylabel("Average Loss")
plt.show()
print(pca_test_loss[-1])
torch.save({
    'epochs': 60,
    'model_state_dict': PCAModel.state_dict(),
    'train_loss': pca_train_loss,
    'test_loss': pca_test_loss
}, 'pca_model.pt')
Standardized Data and Neural Network
d_train = data(standard=True, test=False)
train_loader = torch.utils.data.DataLoader(dataset=d_train, batch_size=500)
d_test = data(standard=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
criterion = nn.SmoothL1Loss()
StandardModel = BatchNormalizationNN()
optimizer = torch.optim.Adadelta(StandardModel.parameters())
stan_train_loss, stan_test_loss = train(train_loader, test_loader, criterion, StandardModel, optimizer, 250)
plt.plot(stan_train_loss)
plt.title("Standardization Train")
plt.xlabel("Epochs")
plt.ylabel("Average Loss")
plt.show()
print(stan_train_loss[-1])
plt.plot(stan_test_loss)
plt.title("Standardization Validation Data ")
plt.xlabel("Epochs")
plt.ylabel("Average Loss")
plt.show()
print(stan_test_loss[-1])
torch.save({
    'epochs': 250,
    'model_state_dict': StandardModel.state_dict(),
    'train_loss': stan_train_loss,
    'test_loss': stan_test_loss
}, 'standardized_model.pt')

# Viewing the Results. The code below will allow you to see the predicted price,
# actual price, and error for each house in the test dataset.We will use the
# absolute Loss.
criterion = nn.SmoothL1Loss()
limit = 9
d_test = data(regular=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
​
for i, (x, y) in enumerate(test_loader):
    RegularModel.eval()
    z = RegularModel(x)
    loss = criterion(z, y)
    print(i)
    print('Predicted: ',z.item())
    print('Actual: ', y.item())
    print('Loss: ', loss.item())
    if (i == limit):
        break
d_test = data(pca=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
​
for i, (x, y) in enumerate(test_loader):
    PCAModel.eval()
    z = PCAModel(x)
    loss = criterion(z, y)
    print(i)
    print('Predicted: ',z.item())
    print('Actual: ', y.item())
    print('Loss: ', loss.item())
    if (i == limit):
        break
d_test = data(standard=True, test=True)
test_loader = torch.utils.data.DataLoader(dataset=d_test)
​
for i, (x, y) in enumerate(test_loader):
    StandardModel.eval()
    z = StandardModel(x)
    loss = criterion(z, y)
    print(i)
    print('Predicted: ',z.item())
    print('Actual: ', y.item())
    print('Loss: ', loss.item())
    if (i == limit):
        break
