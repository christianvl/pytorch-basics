# cognitiveclass.ai logo
# Deep Autoencoder- Noisey manifold
# Table of Contents

# In this lab, you will create a deep autoencoder for noise removal in a non-linear manifold..

#     Make Some Data
#     Function to Train, the Model
#     Build Custom module
#     Training and Validate Model

# Estimated Time Needed: 20 min
# Preparation

# We'll need the following libraries

# Import the libraries we need for the la​
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.utils.data import Dataset, DataLoader

torch.manual_seed(0)

# Create a noisy manifold dataset class example:
# Create Data object
class Data(Dataset):
    # Constructor
    def __init__(self, N_SAMPLES=400, noise_std=0.1, train=True):
        self.x=torch.zeros(( N_SAMPLES,2))
        self.x_=torch.zeros(( N_SAMPLES,2))
        self.x[:,0] = torch.linspace(-2, 2, N_SAMPLES)
        self.x[:,1] = (self.x[:,0] **3)
        self.len=N_SAMPLES
        if train != True:
            torch.manual_seed(1)
            self.x_[:,0] = self.x[:,0]+ noise_std * torch.randn(N_SAMPLES)
            self.x_[:,1] = self.x[:,1]+ noise_std * torch.randn(N_SAMPLES)
        else:
            torch.manual_seed(0)
            self.x_[:,0] = self.x[:,0]+ noise_std * torch.randn(N_SAMPLES)
            self.x_[:,1] = self.x[:,1]+ noise_std * torch.randn(N_SAMPLES)
    # Getter
    def __getitem__(self, index):
        return self.x_[index,:], self.x[index,:]
    # Get Length
    def __len__(self):
        return self.len
    # Plot the data
    def plot(self):
        plt.figure(figsize = (6.1, 10))
        plt.scatter(self.x_[:,0].numpy(),self.x_[:,1].numpy(), label="Noisy function")
        plt.plot(self.x[:,0].numpy(), self.x[:,1].numpy() ,label="True Function", color='orange')
        plt.xlabel("x_{1}")
        plt.ylabel("x_{2}")
        plt.xlim((-1, 1))
        plt.ylim((-2, 2.5))
        plt.legend(loc="best")
        plt.show()

# This example will be used to plot the data.
def plot_points(model,dataset,title="autoencoder"):
    xhat_points=model(dataset.x_)
    f_xat=model(dataset.x)
    ax=plt.subplot(121)
    ax.scatter(xhat_points[:,0].detach().numpy(),xhat_points[:,1].detach().numpy(), label="xhat")
    ax.scatter(dataset.x_[:,0].numpy(),dataset.x_[:,1].numpy(), label="xhat")
    ax.plot(f_xat[:,0].detach().numpy(), f_xat[:,1].detach().numpy() ,label="fhat", color='r')
    ax.plot(dataset.x[:,0].numpy(), dataset.x[:,1].numpy(),'g' ,label="f",)
    ax.set_title(title)
    ax.legend()
    plt.show()

# Make Some Data
# Create a dataset object for training:
# # Create the dataset object and plot the dataset
train_dataset = Data(N_SAMPLES=1000, noise_std=0.1)
train_dataset .plot()

# Create a dataset object for validation data:
# Create validation dataset object
validation_dataset = Data(N_SAMPLES=400, noise_std=0.1,train=False)
len(train_dataset)

# Validiaon loader:
train_loader = torch.utils.data.DataLoader(dataset=train_dataset , batch_size=2)
validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset, batch_size=2)

# Create the Model, Optimizer, and Total Loss Function (Cost)
# Create a function to train the autoencoder:
def train_model(model,train_loader,validation_loader,optimizer,n_epochs=4):
    #global variable
    cost_list_training =[]
    cost_list_validation =[]
    for epoch in range(n_epochs):
        cost_training=0
        for x_tilde, x in train_loader:
            model.train()
            optimizer.zero_grad()
            xhat = model(x_tilde)
            loss = criterion(xhat , x)
            loss.backward()
            optimizer.step()
            cost_training+=loss.data
        cost_list_training.append(cost_training)
        #perform a prediction on the validation  data
        cost_val=0
        for x_test, y_test in validation_loader:
            model.eval()
            z = model(x_test)
            loss = criterion(z, x_test)
            cost_val+=loss.data
        cost_list_validation.append(cost_val)
    return cost_list_training, cost_list_validation

# This function will plot the training cost and validation cost.
def plot_train_val(cost_list,accuracy_list,val_data_label ='accuracy'):
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.plot(cost_list, color = color)
    ax1.set_xlabel('epoch ', color = color)
    ax1.set_ylabel('total loss', color = color)
    ax1.tick_params(axis = 'y', color = color)
    ax2 = ax1.twinx()
    color = 'tab:blue'
    ax2.set_ylabel(val_data_label, color = color)  # we already handled the x-label with ax1
    ax2.plot(accuracy_list, color = color)
    ax2.tick_params(axis = 'y', color = color)
    fig.tight_layout()
    plt.show()

# Build Custom module
# Create Autoencoder custom modules:
# Linear autoencoder:
class AutoEncoder(nn.Module):
    # Contructor
    def __init__(self, input_dim=2, encoding_dim=2):
        super(AutoEncoder, self).__init__()
        self.encoder = nn.Linear(input_dim,encoding_dim)
        self.decoder = nn.Linear(encoding_dim,input_dim)
    # Prediction
    def forward(self, x):
        x =  self.encoder(x)
        x=self.decoder(x)
        return x
    def code(self,x):
        return self.encoder(x)

    # Shallow Nonlinear Decoding and Encoding Function
class AutoEncoder_s(nn.Module):
    def __init__(self, input_dim=2, encoding_dim=2):
        super(AutoEncoder_s, self).__init__()
        self.encoder = nn.Linear(input_dim,encoding_dim)
        self.decoder = nn.Linear(encoding_dim,input_dim)
    # Prediction
    def forward(self, x):
        x =  torch.sigmoid(self.encoder(x))
        x=torch.selu(self.decoder(x))
        return x
    def code(self,x):
        return self.encoder(x)

# Deep autoencoder Nonlinear Decoding and Encoding Function
class Autoencoderone_hidden(nn.Module):
    def __init__(self, input_dim=2,encoding_dim_1=2,encoding_dim_2=2):
        super(Autoencoderone_hidden,self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, encoding_dim_1),
            nn.Tanh(),
            nn.Linear(encoding_dim_1, encoding_dim_2))
        self.decoder = nn.Sequential(
            nn.Linear(encoding_dim_2, encoding_dim_1),
            nn.Tanh(),
            nn.Linear(encoding_dim_1, input_dim))
    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

# Training and Validate Model
# In this section, we will train and validate the different autoencoders. We have the linear autoencoder:
model_linear=AutoEncoder(2,1000)
criterion = nn.MSELoss()
learning_rate = 0.01
optimizer = torch.optim.Adam(model_linear.parameters(), lr = learning_rate)
accuracy_list, lost_list=train_model(model=model_linear,n_epochs=10,train_loader=train_loader,validation_loader=validation_loader,optimizer=optimizer)
plot_train_val(accuracy_list, lost_list,val_data_label ='Cost')
plot_points(model_linear,validation_dataset,"linear overcomplete ")

# we have the shallow autoencoder
model_relu=AutoEncoder_s(2,1000)
criterion = nn.MSELoss()
learning_rate = 0.001
optimizer = torch.optim.Adam(model_relu.parameters(), lr = learning_rate)
accuracy_list, lost_list=train_model(model=model_relu,n_epochs=10,train_loader=train_loader,validation_loader=validation_loader,optimizer=optimizer)
plot_train_val(accuracy_list, lost_list,val_data_label ='Cost')
plot_points(model_relu,validation_dataset,"non-linear overcomplete ")

# finally, we have the deep autoencoder
model=Autoencoderone_hidden(2,20,20)
criterion = nn.MSELoss()
learning_rate = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
accuracy_list, lost_list=train_model(model=model,n_epochs=20,train_loader=train_loader,validation_loader=validation_loader,optimizer=optimizer)
plot_train_val(accuracy_list, lost_list,val_data_label ='Cost')
plt.show()
plot_points(model,validation_dataset,"deep overcomplete ")

# We see the deep autoencoder performs better on the training and validation data.
