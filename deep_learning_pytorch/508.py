# cognitiveclass.ai logo
# Deep Autoencoders for Noise Removal
# Table of Contents

# In this lab, you will train a Deep Autoencoder to remove noise

#     Imports and Utility Functions
#     Load Data
#     Deep Autoencoder
#     Define Criterion function, Optimizer and Train the Model
#     Results

# Estimated Time Needed: 25 min
# Imports and Utility Functions

# Import the libraries we need to use in this lab.

# !pip install Pillow==6.2.2

​

# Using the following line code to install the torchvision library

# !conda install -y torchvision
​
import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.datasets as dsets
import matplotlib.pylab as plt
import numpy as np
import copy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as colors
torch.manual_seed(7)
​
# Requirement already satisfied: Pillow==6.2.2 in /Users/joseph/anaconda/lib/python3.6/site-packages (6.2.2)
# <torch._C.Generator at 0x109be87d0>
# download the model
! wget https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DL0110EN/models%20/autoencoders/deepauto_image_noise_removal.pt

# function to plot data
def show_data(data_sample, y=None):
    plt.imshow(data_sample[0].detach().numpy().reshape(IMAGE_SIZE, IMAGE_SIZE), cmap='gray')
    plt.show()

# Plot training and validation data
def plot_train_val(cost_list,accuracy_list,val_data_label ='Validation error '):
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.plot(cost_list, color = color)
    ax1.set_xlabel('epoch ', color = color)
    ax1.set_ylabel('total loss', color = color)
    ax1.tick_params(axis = 'y', color = color)
    ax2 = ax1.twinx()
    color = 'tab:blue'
    ax2.set_ylabel(val_data_label, color = color)  # we already handled the x-label with ax1
    ax2.plot(accuracy_list, color = color)
    ax2.tick_params(axis = 'y', color = color)
    fig.tight_layout()
    plt.show()

# Plot images, images with noise and images after passed through the autoencoders.
def plot_autoencoder(model,dataset,noise_std,samples=[1,23,45]):
    for sample in samples:
        x=validation_dataset[sample][0]
        x_=x+noise_std*torch.randn((1,16,16))
        xhat=model(x_.view(-1,256))
        plt.figure()
        plt.subplot(131)
        plt.imshow(x.detach().numpy().reshape(IMAGE_SIZE, IMAGE_SIZE), cmap='gray')
        plt.title('Original image')
        plt.subplot(132)
        plt.imshow(x_.detach().numpy().reshape(IMAGE_SIZE, IMAGE_SIZE), cmap='gray')
        plt.title('noisy image')
        plt.subplot(133)
        plt.imshow(xhat.detach().numpy().reshape(IMAGE_SIZE, IMAGE_SIZE), cmap='gray')
        plt.title('output of autoencoder')

# Load Data
# we create a transform to resize the image and convert it to a tensor :
IMAGE_SIZE = 16
tensor_size=IMAGE_SIZE*IMAGE_SIZE
composed = transforms.Compose([transforms.Resize((IMAGE_SIZE, IMAGE_SIZE)), transforms.ToTensor()])

# Load the training dataset by setting the parameters train to True. We use the
# transform defined above.
train_dataset = dsets.FashionMNIST(root='./data', train=True, download=True, transform=composed)
​
# Load the testing dataset by setting the parameters train False.
# Make the validating
validation_dataset = dsets.FashionMNIST(root='./data', train=False, download=True, transform=composed)

# We can see the data type is long.
# Show the data type for each element in dataset
train_dataset[0][1].type()

# Each element in the rectangular tensor corresponds to a number representing a
# pixel intensity as demonstrated by the following image.

# MNIST data image

# Print out the fourth label
# The label for the fourth data element
train_dataset[3][1]
noise_std=0.01

# Plot the fourth sample
# The image for the fourth data element
show_data(train_dataset[3])

# create a train loader and data loader object.
train_batch_size=100
validation_batch_size=5000
train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=train_batch_size)
validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset, batch_size=validation_batch_size)

# Deep Autoencoder

# In this section, we build an autoencoder class or custom module with one
# layer. We also Build a function to train it using the mean square error.
class Autoencoderone_hidden(nn.Module):
    def __init__(self, input_dim=2,encoding_dim_1=2,encoding_dim_2=2):
        super(Autoencoderone_hidden,self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, encoding_dim_1),
            nn.ReLU(),
            nn.Linear(encoding_dim_1, encoding_dim_2),nn.ReLU())
        self.decoder = nn.Sequential(
            nn.Linear(encoding_dim_2, encoding_dim_1),
            nn.ReLU(),
            nn.Linear(encoding_dim_1, input_dim))
​
    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

    def code(self,x):
        return self.encoder(x)

# This method trains the autoencoder; the parameter model is the autoencoder
# object. The parameter train_loader and validation_loader is the train loader
# and validation loader. The Parameter optimizer is the optimizer object, and
# n_epoch is the number of epochs
def train_model(model,train_loader,validation_loader,optimizer,n_epochs=4,noise_std=0.1, train_batch_size=100,validation_batch_size=5000,checkpoint_path=None,checkpoint=None):
    #global variable
    cost_list_training =[]
    cost_list_validation =[]
    for epoch in range(n_epochs):
        cost_training=0
        for x, y in train_loader:
            model.train()
            optimizer.zero_grad()
            x_ =x+noise_std*torch.randn((train_batch_size,1,16,16))
            z = model(x_.view(-1,256))
            loss = criterion(z, x.view(-1,256))
            loss.backward()
            optimizer.step()
            cost_training+=loss.data
        cost_list_training.append(cost_training)
        print("epoch {}, Cost {}".format(epoch+1,cost_training) )
        #perform a prediction on the validation  data
        cost_val=0
        for x_test, y_test in validation_loader:
            model.eval()
            x_ =x_test+noise_std*torch.randn((validation_batch_size,1,16,16))
            z = model(x_.view(-1,256))
            loss = criterion(z, x_test.view(-1,256))
            cost_val+=loss.data
        cost_list_validation.append(cost_val)
    if checkpoint:
        checkpoint['epoch']=epoch
        checkpoint['model_state_dict']=model.state_dict()
        checkpoint['optimizer_state_dict']= optimizer.state_dict()
        checkpoint['loss']=loss
        checkpoint['training_cost']=cost_list_training
        checkpoint['validaion_cost']=cost_list_validation
        torch.save(checkpoint, checkpoint_path)
    return cost_list_training, cost_list_validation

# Results
# We train the model using the Root means square error to remove the noise. We create an autoencoder object
# We create the model, criterion and optimizer:
model= Autoencoderone_hidden(input_dim=tensor_size ,encoding_dim_1=500,encoding_dim_2=100)
criterion = nn.MSELoss()
learning_rate = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
# Uncomment to train the model yourself; otherwise, you can load the model on the next line of code.
# checkpoint={'epoch':None,'model_state_dict':None ,'optimizer_state_dict':None ,'loss': None ,'training_cost':None,'validaion_cost':None } checkpoint_path='deepauto_image_noise_removal.pt' cost_list_training, cost_list_validation=train_model(model,train_loader,validation_loader,optimizer,n_epochs=20,noise_std=0.15,checkpoint_path=checkpoint_path,checkpoint=checkpoint)

# we will load the checkpoint from memory
checkpoint_path='deepauto_image_noise_removal.pt'
checkpoint= torch.load(checkpoint_path)

# load the model
model.load_state_dict(checkpoint['model_state_dict'])

# load the training and validation cost
cost_list_training, cost_list_validation= checkpoint['training_cost'], checkpoint['validaion_cost']

# display the result
plot_train_val(cost_list_training, cost_list_validation)
plot_autoencoder(model,validation_dataset,noise_std=0.2,samples=[1,23,45])
