# cognitiveclass.ai logo
# Deep Autoencoder for Anomaly Detection
# Table of Contents

# In this lab, we will use deep autoencoders for Anomaly detection.

#     Imports and Utility Functions
#     Load Data
#     Autoencoder
#     Define Criterion function, Optimizer and Train the Model
#     Analyze Results

# Estimated Time Needed: 25 min
# Imports and Utility Functions

# Import the libraries we need to use in this lab.

# !pip install Pillow==6.2.2

# Using the following line code to install the torchvision library

# !conda install -y torchvision
import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.datasets as dsets
import matplotlib.pylab as plt
import numpy as np
import copy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as colors
torch.manual_seed(7)

# we download the model
# ! wget https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DL0110EN/models%20/autoencoders/autoencoder_anomaly.pt

# in this function, we will plot the image
def show_data(data_sample, y=None):
    plt.imshow(data_sample[0].detach().numpy().reshape(IMAGE_SIZE, IMAGE_SIZE), cmap='gray')
    plt.show()

# This function will plot the training and validation cost.
def plot_train_val(cost_list,accuracy_list,val_data_label ='Validation error '):
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.plot(cost_list, color = color)
    ax1.set_xlabel('epoch ', color = color)
    ax1.set_ylabel('total loss', color = color)
    ax1.tick_params(axis = 'y', color = color)
    ax2 = ax1.twinx()
    color = 'tab:blue'
    ax2.set_ylabel(val_data_label, color = color)  # we already handled the x-label with ax1
    ax2.plot(accuracy_list, color = color)
    ax2.tick_params(axis = 'y', color = color)
    fig.tight_layout()
    plt.show()

# Load the Data
# we create a transform to resize the image and convert it to a tensor :
IMAGE_SIZE = 16
tensor_size=IMAGE_SIZE*IMAGE_SIZE
composed = transforms.Compose([transforms.Resize((IMAGE_SIZE, IMAGE_SIZE)), transforms.ToTensor()])

# Load the training dataset by setting the parameters train to True. We use the transform defined above.
train_dataset = dsets.FashionMNIST(root='./data', train=True, download=True, transform=composed)

# Load the testing dataset by setting the parameters train False.
# Make the validating

validation_dataset = dsets.FashionMNIST(root='./data', train=False, download=True, transform=composed)

# We can see the data type is long.
# Show the data type for each element in dataset
train_dataset[0][1].type()

# Each element in the rectangular tensor corresponds to a number representing a pixel intensity as demonstrated by the following image.
# MNIST data image
# Print out the fourth label
# The label for the fourth data element
train_dataset[3][1]
noise_std=0.01

# Plot the fourth sample
# The image for the fourth data element
show_data(train_dataset[3])

# create a train loader and data loader object.
train_batch_size=100
validation_batch_size=5000
train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=train_batch_size)
validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset, batch_size=validation_batch_size)

# AutoEncoder In this section, we build an autoencoder class or custom module
# with one layer. We also Build a function to train it using the mean square
# error.
class Autoencoderone_hidden(nn.Module):
    def __init__(self, input_dim=2,encoding_dim_1=2,encoding_dim_2=2):
        super(Autoencoderone_hidden,self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, encoding_dim_1),
            nn.ReLU(),
            nn.Linear(encoding_dim_1, encoding_dim_2),nn.ReLU())
        self.decoder = nn.Sequential(
            nn.Linear(encoding_dim_2, encoding_dim_1),
            nn.ReLU(),
            nn.Linear(encoding_dim_1, input_dim))

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
    def code(self,x):
        return self.encoder(x)

# This method trains the autoencoder; the parameter model is the autoencoder
# object. The parameter train_loader and validation_loader is the train loader
# and validation loader. The Parameter optimizer is the optimizer object, and
# n_epoch is the number of epochs
def train_model(model,train_loader,validation_loader,optimizer,n_epochs=4,checkpoint_path=None,checkpoint=None):
    #global variable
    cost_list_training =[]
    cost_list_validation =[]
    for epoch in range(n_epochs):
        cost_training=0
        for x, y in train_loader:
            model.train()
            optimizer.zero_grad()
            z = model(x.view(-1,256))
            loss = criterion(z, x.view(-1,256))
            loss.backward()
            optimizer.step()
            cost_training+=loss.data
        cost_list_training.append(cost_training)
        print("epoch {}, Cost {}".format(epoch+1,cost_training) )
        #perform a prediction on the validation  data
        cost_val=0
        for x_test, y_test in validation_loader:
            model.eval()
            z = model(x.view(-1,256))
            loss = criterion(z, x.view(-1,256))
            cost_val+=loss.data
        cost_list_validation.append(cost_val)
        if checkpoint:
            checkpoint['epoch']=epoch
            checkpoint['model_state_dict']=model.state_dict()
            checkpoint['optimizer_state_dict']= optimizer.state_dict()
            checkpoint['loss']=loss
            checkpoint['training_cost']=cost_list_training
            checkpoint['validaion_cost']=cost_list_validation
            torch.save(checkpoint, checkpoint_path)
    return cost_list_training, cost_list_validation

# Visualizing the Code
# We create an autoencoder object, criterion function and optimizer.
model= Autoencoderone_hidden(input_dim=tensor_size ,encoding_dim_1=400,encoding_dim_2=100)
criterion = nn.MSELoss()
learning_rate = 0.0001
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)

# Uncomment to train the model yourself; otherwise, you can load the model on the next line of code.
# checkpoint={'epoch':None,'model_state_dict':None ,'optimizer_state_dict':None ,'loss': None ,'training_cost':None,'validaion_cost':None } checkpoint_path='autoencoder_anomaly.pt' cost_list_training, cost_list_validation=train_model(model,train_loader,validation_loader,optimizer,n_epochs=10,checkpoint_path=checkpoint_path,checkpoint=checkpoint)
# we load the model and checkpoints:
checkpoint_path='autoencoder_anomaly.pt'
checkpoint= torch.load(checkpoint_path)
model.load_state_dict(checkpoint['model_state_dict'])

# We load and plot the cost for the training and validation data:
cost_list_training, cost_list_validation=checkpoint['training_cost'], checkpoint['validaion_cost']
plot_train_val(cost_list_training, cost_list_validation)

# We then calculate the loss for each sample in the validation data:
val_error=[nn.MSELoss()(x.squeeze().view(-1,256), model(x.squeeze().view(-1,256))).item() for x,_ in validation_dataset ]

# The samples with the 10 larget loss are given by:
largest=np.argsort(val_error)[-10:]

# we plot out the samples:
for l in largest:
    show_data(validation_dataset[l][0])
